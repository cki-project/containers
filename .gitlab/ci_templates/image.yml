---
include:
  - local: .gitlab/ci_templates/internal-runners.yml
    rules: [{if: $VISIBILITY == 'internal'}]
  - local: .gitlab/ci_templates/local-publish.yml

.trigger_dependency_mr_local:
  extends: .trigger_dependency_mr
  stage: 💉

# run the unit tests of pipeline-definition
test-pipeline-definition:
  extends: .trigger_dependency_mr_local
  trigger: {project: cki-project/pipeline-definition, branch: production}
  variables:
    ONLY_JOB_NAME: $IMAGE_NAME
    image_tag: p-$PARENT_PIPELINE_ID
  rules:
    - if: $IMAGE_NAME =~ /^(builder-.*|python)$/

# run build-only CKI pipelines based on the `cki-gating` branch in kernel-ark
test-kernel-ark:
  extends: .trigger_dependency_mr_local
  trigger: {project: cki-project/kernel-ark, branch: cki-gating}
  variables:
    JOB_FILTER: /$IMAGE_NAME/
    builder_image_tag: p-$PARENT_PIPELINE_ID
  rules:
    - if: $IMAGE_NAME =~ /^(builder-rawhide|builder-eln|builder-stream10)$/

# build derived images
test-base:
  extends: .trigger_dependency_mr_local
  trigger: {project: cki-project/$CONTAINERS_DOWNSTREAM_PROJECT, branch: production}
  variables:
    FORCE_PUBLISH: 'true'
    BASE_IMAGE_TAG: p-$PARENT_PIPELINE_ID
  parallel:
    matrix:
      - CONTAINERS_DOWNSTREAM_PROJECT:
          - cki-tools
          - datawarehouse
          - kernel-workflow
          - reporter
  rules:
    - if: $IMAGE_NAME == 'base'
      changes: [includes/snippet-fedora-version]
    - if: $IMAGE_NAME == 'base'
      when: manual
      allow_failure: true

# build a simple test image via `buildah`
.test_buildah:
  extends: [.publish]
  image: quay.io/cki/${IMAGE_NAME}:p-${PARENT_PIPELINE_ID}
  stage: 💉
  tags:
    - $RUNNER_TAG
  variables:
    # reset variables from parent pipeline
    REGISTRY: 'dummy'
  before_script:
    - rm -r includes           # force using the includes from the image
    - export IMAGE_NAME="smoke-${CI_JOB_NAME%%:*}"
    - cp builds/smoke.in "builds/${IMAGE_NAME}.in"
  rules:
    - if: $IMAGE_NAME == 'buildah' && $IMAGE_ARCHES =~ $IMAGE_ARCH

test-buildah-public-emulated:
  extends: [.test_buildah, .arch_matrix_public_emulated]

test-buildah-public-native:
  extends: [.test_buildah, .arch_matrix_public_native]

test-buildah-internal-emulated:
  extends: [.test_buildah, .arch_matrix_internal_emulated]

test-buildah-internal-emulated-staging:
  extends: [.test_buildah, .arch_matrix_internal_emulated_staging]

test-buildah-internal-native:
  extends: [.test_buildah, .arch_matrix_internal_native]

# guard against https://github.com/containers/buildah/issues/6001
test_buildah_issue_6001:
  extends: [.publish, .arch_matrix_public_native]
  image: quay.io/cki/${IMAGE_NAME}:p-${PARENT_PIPELINE_ID}
  stage: 💉
  tags:
    - $RUNNER_TAG
  variables:
    # reset variables from parent pipeline
    REGISTRY: 'dummy'
  before_script:
    - rm -r includes           # force using the includes from the image
    - export IMAGE_NAME=buildah-issue-6001
  rules:
    - if: $IMAGE_NAME == 'buildah' && $IMAGE_ARCHES =~ $IMAGE_ARCH

# check that the quay.io container repository is correctly set to public/private
test-quay-visibility:
  extends: .publish
  stage: 💉
  script:
    - |
      # Checking image visibility
      if [[ $IMAGE_NAME = buildah ]] ; then
        dnf -y --setopt=install_weak_deps=False install skopeo
      fi
      if skopeo inspect --raw docker://quay.io/cki/${IMAGE_NAME}:p-${PARENT_PIPELINE_ID} > /dev/null; then
        visibility=public
      else
        visibility=internal
      fi
      if [[ ${VISIBILITY} != ${visibility} ]]; then
        echo -e "\e[1;31mImage was expected to be ${VISIBILITY}, but was found to be ${visibility}\e[0m"
        exit 1
      fi
  retry:
    max: 2
    when: always
